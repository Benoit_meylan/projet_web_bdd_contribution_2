-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 07 Juin 2018 à 10:03
-- Version du serveur :  5.6.39-83.1-log
-- Version de PHP :  5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hapygest_web`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`hapygest_admin`@`localhost` PROCEDURE `ajout_contenance` ()  BEGIN
DECLARE crs INT DEFAULT 0;

WHILE crs < 250 DO
SET crs = crs + 50;
INSERT INTO `Contenance`(`Contenance`) VALUES (crs);
END WHILE;
END$$

CREATE DEFINER=`hapygest_admin`@`localhost` PROCEDURE `ajout_pointure` ()  BEGIN
DECLARE prs INT DEFAULT 15;

WHILE prs < 46 DO
SET prs = prs + 1;
INSERT INTO `Pointure`(`pointure`) VALUES (prs);
END WHILE;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Adresses`
--

CREATE TABLE `Adresses` (
  `idAdresses` int(11) NOT NULL,
  `adresse` varchar(45) DEFAULT NULL,
  `pays` varchar(45) DEFAULT NULL,
  `ville` varchar(45) DEFAULT NULL,
  `npa` int(11) DEFAULT NULL,
  `fkUtilisateurs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Categories`
--

CREATE TABLE `Categories` (
  `idCategories` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Categories`
--

INSERT INTO `Categories` (`idCategories`, `nom`) VALUES
(1, 'vetements'),
(2, 'parfum'),
(3, 'chaussures');

-- --------------------------------------------------------

--
-- Structure de la table `Commande`
--

CREATE TABLE `Commande` (
  `idCommande` int(11) NOT NULL,
  `fkUtilisateurs` int(11) NOT NULL,
  `dateCommande` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Contenance`
--

CREATE TABLE `Contenance` (
  `idContenance` int(11) NOT NULL,
  `contenance` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Contenance`
--

INSERT INTO `Contenance` (`idContenance`, `contenance`) VALUES
(1, 50),
(2, 100),
(3, 150),
(4, 200),
(5, 250);

-- --------------------------------------------------------

--
-- Structure de la table `Contenance_des_Produits`
--

CREATE TABLE `Contenance_des_Produits` (
  `fkContenance` int(11) NOT NULL,
  `fkProduits` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Contenance_des_Produits`
--

INSERT INTO `Contenance_des_Produits` (`fkProduits`, `fkContenance`, `stock`) VALUES
(1, 2, 123);

-- --------------------------------------------------------

--
-- Structure de la table `Images`
--

CREATE TABLE `Images` (
  `idImages` int(11) NOT NULL,
  `fkProduits` int(11) NOT NULL,
  `lien_image1` varchar(255) DEFAULT NULL,
  `lien_image2` varchar(255) DEFAULT NULL,
  `lien_image3` varchar(255) DEFAULT NULL,
  `lien_image4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Images`
--

INSERT INTO `Images` (`idImages`, `fkProduits`, `lien_image1`, `lien_image2`, `lien_image3`, `lien_image4`) VALUES
(1, 1, 'data/pictures/1_0.jpg', 'data/pictures/1_1.jpg', 'data/pictures/1_2.jpg', '');

-- --------------------------------------------------------

--
-- Structure de la table `Pointure`
--

CREATE TABLE `Pointure` (
  `idPointure` int(11) NOT NULL,
  `pointure` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Pointure`
--

INSERT INTO `Pointure` (`idPointure`, `pointure`) VALUES
(1, 16),
(2, 17),
(3, 18),
(4, 19),
(5, 20),
(6, 21),
(7, 22),
(8, 23),
(9, 24),
(10, 25),
(11, 26),
(12, 27),
(13, 28),
(14, 29),
(15, 30),
(16, 31),
(17, 32),
(18, 33),
(19, 34),
(20, 35),
(21, 36),
(22, 37),
(23, 38),
(24, 39),
(25, 40),
(26, 41),
(27, 42),
(28, 43),
(29, 44),
(30, 45),
(31, 46);

-- --------------------------------------------------------

--
-- Structure de la table `Pointure_des_Produits`
--

CREATE TABLE `Pointure_des_Produits` (
  `fkPointure` int(11) NOT NULL,
  `fkProduits` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Produits`
--

CREATE TABLE `Produits` (
  `idProduits` int(11) NOT NULL,
  `fkCategories` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `prix` decimal(8,2) DEFAULT NULL,
  `description` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Produits`
--

INSERT INTO `Produits` (`idProduits`, `fkCategories`, `nom`, `prix`, `description`) VALUES
(1, 2, 'Pull gris', '123.00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su');

-- --------------------------------------------------------

--
-- Structure de la table `Produits_des_Commandes`
--

CREATE TABLE `Produits_des_Commandes` (
  `fkCommande` int(11) NOT NULL,
  `fkProduits` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Roles`
--

CREATE TABLE `Roles` (
  `idRoles` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Roles`
--

INSERT INTO `Roles` (`idRoles`, `nom`) VALUES
(1, 'client'),
(2, 'vendeur'),
(3, 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `Taille`
--

CREATE TABLE `Taille` (
  `idTaille` int(11) NOT NULL,
  `taille` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Taille`
--

INSERT INTO `Taille` (`idTaille`, `taille`) VALUES
(1, 'XS'),
(2, 'S'),
(3, 'M'),
(4, 'L'),
(5, 'XL'),
(6, 'XXL');

-- --------------------------------------------------------

--
-- Structure de la table `Taille_des_Produits`
--

CREATE TABLE `Taille_des_Produits` (
  `fkTaille` int(11) NOT NULL,
  `fkProduits` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Taille_des_Produits`
--


-- --------------------------------------------------------

--
-- Structure de la table `Utilisateurs`
--

CREATE TABLE `Utilisateurs` (
  `idUtilisateurs` int(11) NOT NULL,
  `fkRoles` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `Civilite` varchar(10) DEFAULT NULL,
  `eMail` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `motDePasse` varchar(45) DEFAULT NULL,
  `nTelephone` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Utilisateurs`
--

INSERT INTO `Utilisateurs` (`idUtilisateurs`, `fkRoles`, `nom`, `prenom`, `Civilite`, `eMail`, `motDePasse`, `nTelephone`) VALUES
(1, 2, 'zmoos', 'leo', 'm', 'S60wBn6PYk1LMHtnGCz3cQ==', 'kb0bhF+rH2dcL5d2P7A3wQ==', 9080),
(2, 2, 'Meylan', 'Benoit', 'm', 'zVnXvlMrysM3Y5g81sxzELvuPeiTuGCFtA0s0fYjleE', 'kb0bhF+rH2dcL5d2P7A3wQ==', 76),
(3, 1, 'Glassey', 'Nicolas', 'm', '43jPF8ToRGQRsPLahn/aJGupKfgoPw7ug1iCyUkPiZU=', 'l3Kb5g+73hN6mkmNTBNTUg==', 2147483647);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Adresses`
--
ALTER TABLE `Adresses`
  ADD PRIMARY KEY (`idAdresses`),
  ADD KEY `fk_Adresses_Utilisateurs1_idx` (`fkUtilisateurs`);

--
-- Index pour la table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`idCategories`);

--
-- Index pour la table `Commande`
--
ALTER TABLE `Commande`
  ADD PRIMARY KEY (`idCommande`),
  ADD KEY `fk_Commande_Utilisateurs1_idx` (`fkUtilisateurs`);

--
-- Index pour la table `Contenance`
--
ALTER TABLE `Contenance`
  ADD PRIMARY KEY (`idContenance`);

--
-- Index pour la table `Contenance_des_Produits`
--
ALTER TABLE `Contenance_des_Produits`
  ADD PRIMARY KEY (`fkProduits`,`fkContenance`),
  ADD KEY `fk_Produits_has_Contenance_Contenance1_idx` (`fkContenance`),
  ADD KEY `fk_Produits_has_Contenance_Produits1_idx` (`fkProduits`);

--
-- Index pour la table `Images`
--
ALTER TABLE `Images`
  ADD PRIMARY KEY (`idImages`),
  ADD KEY `fk_Images_Produits1_idx` (`fkProduits`);

--
-- Index pour la table `Pointure`
--
ALTER TABLE `Pointure`
  ADD PRIMARY KEY (`idPointure`);

--
-- Index pour la table `Pointure_des_Produits`
--
ALTER TABLE `Pointure_des_Produits`
  ADD PRIMARY KEY (`fkPointure`,`fkProduits`),
  ADD KEY `fk_Pointure_has_Produits_Produits1_idx` (`fkProduits`),
  ADD KEY `fk_Pointure_has_Produits_Pointure1_idx` (`fkPointure`);

--
-- Index pour la table `Produits`
--
ALTER TABLE `Produits`
  ADD PRIMARY KEY (`idProduits`),
  ADD KEY `fk_Produits_Categories1_idx` (`fkCategories`);

--
-- Index pour la table `Produits_des_Commandes`
--
ALTER TABLE `Produits_des_Commandes`
  ADD PRIMARY KEY (`fkCommande`,`fkProduits`),
  ADD KEY `fk_Commande_has_Produits_Produits1_idx` (`fkProduits`),
  ADD KEY `fk_Commande_has_Produits_Commande1_idx` (`fkCommande`);

--
-- Index pour la table `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`idRoles`);

--
-- Index pour la table `Taille`
--
ALTER TABLE `Taille`
  ADD PRIMARY KEY (`idTaille`);

--
-- Index pour la table `Taille_des_Produits`
--
ALTER TABLE `Taille_des_Produits`
  ADD PRIMARY KEY (`fkTaille`,`fkProduits`),
  ADD KEY `fk_tailleProduit_has_Produits_Produits1_idx` (`fkProduits`),
  ADD KEY `fk_tailleProduit_has_Produits_tailleProduit1_idx` (`fkTaille`);

--
-- Index pour la table `Utilisateurs`
--
ALTER TABLE `Utilisateurs`
  ADD PRIMARY KEY (`idUtilisateurs`),
  ADD KEY `fk_Utilisateurs_Roles_idx` (`fkRoles`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Adresses`
--
ALTER TABLE `Adresses`
  MODIFY `idAdresses` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `idCategories` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Commande`
--
ALTER TABLE `Commande`
  MODIFY `idCommande` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Contenance`
--
ALTER TABLE `Contenance`
  MODIFY `idContenance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `Images`
--
ALTER TABLE `Images`
  MODIFY `idImages` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `Pointure`
--
ALTER TABLE `Pointure`
  MODIFY `idPointure` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `Produits`
--
ALTER TABLE `Produits`
  MODIFY `idProduits` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `Roles`
--
ALTER TABLE `Roles`
  MODIFY `idRoles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Taille`
--
ALTER TABLE `Taille`
  MODIFY `idTaille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `Utilisateurs`
--
ALTER TABLE `Utilisateurs`
  MODIFY `idUtilisateurs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Adresses`
--
ALTER TABLE `Adresses`
  ADD CONSTRAINT `fk_Adresses_Utilisateurs1` FOREIGN KEY (`fkUtilisateurs`) REFERENCES `Utilisateurs` (`idUtilisateurs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Commande`
--
ALTER TABLE `Commande`
  ADD CONSTRAINT `fk_Commande_Utilisateurs1` FOREIGN KEY (`fkUtilisateurs`) REFERENCES `Utilisateurs` (`idUtilisateurs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Contenance_des_Produits`
--
ALTER TABLE `Contenance_des_Produits`
  ADD CONSTRAINT `fk_Produits_has_Contenance_Contenance1` FOREIGN KEY (`fkContenance`) REFERENCES `Contenance` (`idContenance`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Produits_has_Contenance_Produits1` FOREIGN KEY (`fkProduits`) REFERENCES `Produits` (`idProduits`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Images`
--
ALTER TABLE `Images`
  ADD CONSTRAINT `fk_Images_Produits1` FOREIGN KEY (`fkProduits`) REFERENCES `Produits` (`idProduits`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Pointure_des_Produits`
--
ALTER TABLE `Pointure_des_Produits`
  ADD CONSTRAINT `fk_Pointure_has_Produits_Pointure1` FOREIGN KEY (`fkPointure`) REFERENCES `Pointure` (`idPointure`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pointure_has_Produits_Produits1` FOREIGN KEY (`fkProduits`) REFERENCES `Produits` (`idProduits`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Produits`
--
ALTER TABLE `Produits`
  ADD CONSTRAINT `fk_Produits_Categories1` FOREIGN KEY (`fkCategories`) REFERENCES `Categories` (`idCategories`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Produits_des_Commandes`
--
ALTER TABLE `Produits_des_Commandes`
  ADD CONSTRAINT `fk_Commande_has_Produits_Commande1` FOREIGN KEY (`fkCommande`) REFERENCES `Commande` (`idCommande`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Commande_has_Produits_Produits1` FOREIGN KEY (`fkProduits`) REFERENCES `Produits` (`idProduits`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Taille_des_Produits`
--
ALTER TABLE `Taille_des_Produits`
  ADD CONSTRAINT `fk_tailleProduit_has_Produits_Produits1` FOREIGN KEY (`fkProduits`) REFERENCES `Produits` (`idProduits`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tailleProduit_has_Produits_tailleProduit1` FOREIGN KEY (`fkTaille`) REFERENCES `Taille` (`idTaille`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Utilisateurs`
--
ALTER TABLE `Utilisateurs`
  ADD CONSTRAINT `fk_Utilisateurs_Roles` FOREIGN KEY (`fkRoles`) REFERENCES `Roles` (`idRoles`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
