<?php
/**
 * Created by PhpStorm.
 * User: Leo.ZMOOS
 * Date: 06.06.2018
 * Time: 15:44
 */

$titre = "Hâpy - mon compte";

// ouvre la mémoire tampon
ob_start();

?>


<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-12">
                <section class="section sign-in inner-right-xs">
                    <table class="table col-md-10">
                        <tr>
                            <td><b><u>ID Commande</u></b></td>
                            <td><b><u>Nom</u></b></td>
                            <td><b><u>Date Commande</u></b></td>
                            <td><b><u>Adresse</u></b></td>
                            <td><b><u>Détails</u></b></td>
                        </tr>

                        <?php foreach ($resultats as $item):?>
                            <tr>
                                <td>
                                    <?=$item['idCommande']?>
                                </td>
                                <td>
                                    <?=$item['nom']?>
                                </td>
                                <td>
                                    <?=$item['dateCommande']?>
                                </td>
                                <td>
                                   <?=$item['adresse'].", ".$item['npa']." ".$item['ville']." ".$item['pays']?>
                                </td>
                                <td>
                                    <a href='index.php?action=view_detail_command&idCommand=<?=$item['idCommande']?>&idUser=<?=$item['idUtilisateurs']?>'>détail</a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                </section>
            </div>
        </div>
    </div>
</main>



<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
