<?php
/**
 * Created by PhpStorm.
 * User: Benoît Meylan & Léo Zmoos
 * Date: 11.05.2018
 * Time:  22:02
 */

$titre = "Hâpy - Ajout d'article";
// ouvre la mémoire tampon
ob_start();
?>
<head>
    <script>

        function fnCategory() {

            var cat = document.getElementById("category");

            var contenance = document.getElementById("contenance");
            var taille = document.getElementById("taille");
            var pointure = document.getElementById("pointure");
            switch(cat.value)
            {
                case "1":
                    contenance.hidden = true;
                    taille.hidden = false;
                    pointure.hidden = true;
                    break;
                case "2":
                    contenance.hidden = false;
                    taille.hidden = true;
                    pointure.hidden = true;
                    break;
                case "3":
                    contenance.hidden = true;
                    taille.hidden = true;
                    pointure.hidden = false;
                    break;
            }
        }
    </script>
    <style>
        @import "http://fonts.googleapis.com/css?family=Droid+Sans";
        form{
            background-color:#fff
        }

        #formdiv{
            width:500px;
            float:left;
            text-align:center
        }
        form{
            padding:40px 20px;
            box-shadow:0 0 10px;
            border-radius:2px
        }
        h2{
            margin-left:30px
        }
        .upload{
            background-color:#030E33;
            border:1px solid #030E33;
            border-radius:5px;
            padding:10px;
            color: white;
            box-shadow:2px 2px 15px rgba(0,0,0,.75)
        }
        .upload:hover{
            cursor:pointer;
            background:#c20b0b;
            border:1px solid #c20b0b;
            box-shadow:0 0 5px rgba(0,0,0,.75)
        }
        #file{
            color:green;
            padding:5px;
            border:1px dashed #123456;
            background-color:#f9ffe5
        }
        #upload{
            margin-left:45px
        }
        #noerror{
            color:green;
            text-align:left
        }
        #error{
            color:red;
            text-align:left
        }
        #img{
            width:17px;
            border:none;
            height:17px;
            margin-left:-20px;
            margin-bottom:91px;
        }
        .abcd{
            text-align:center;

        }
        .abcd img{
            height:100px;
            width:100px;
            padding:5px;
            border:1px solid #e8debd;

        }
        b{
            color:red
        }
    </style>
</head>
<br>
<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-lg-12">
                <section class="section sign-in inner-right-xs">
                    <legend>Ajouter un article</legend>
                            <form class="well form-horizontal" enctype="multipart/form-data" action="index.php?action=add_article" method="post">
                                <fieldset>
                                    <!-- Form Name -->
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Nom & Prix de l'article</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <div class="input-group">
                                                            <input  name="name" placeholder="Insérer le nom de l'article" class="form-control"  type="text" maxlength="45" required>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="input-group">
                                                            <input name="price" placeholder="Insérer le prix" class="form-control"  type="number" max="100000" required>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Categorie</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="category" id="category" onchange="fnCategory()" required>
                                                            <?php foreach ($resultats as $resultat) :?>
                                                                <option value="<?=$resultat['idCategories']?>"><?= $resultat['nom'] ?></option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <!-- options dynamiques des produits -->
                                    <div class="form-group" id="contenance">
                                        <label class="col-md-12 control-label">contenance (en ml)</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="contenance" required onchange="fnCategory()">
                                                            <?php for ($i = 50; $i < 300;$i+=50):?>
                                                                <option><?=$i?></option>
                                                            <?php endfor;?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="form-group" id="taille">
                                        <label class="col-md-12 control-label">taille</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="taille" required onchange="fnCategory()">
                                                            <option value="XS">XS</option>
                                                            <option value="XS">S</option>
                                                            <option value="M">M</option>
                                                            <option value="L">L</option>
                                                            <option value="XL">XL</option>
                                                            <option value="XXL">XXL</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="form-group" id="pointure">
                                        <label class="col-md-12 control-label">pointure</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="pointure" required>
                                                            <?php for ($i = 16; $i < 47;$i++):?>
                                                                <option><?=$i?></option>
                                                            <?php endfor;?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- fin des options dynamiques des produits -->

                                    <div class="form-group">
                                        <label class="col-md-12 control-label">pièces en stock</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td><input type="number" class="form-control" name="stock" required></td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12 control-label">description</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <table class="table">
                                                <tr>
                                                    <td><textarea name="description" maxlength="300" class="form-control mb-4" rows="5" style="height: 10em!important;"></textarea></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>


                                    <h4>Ajouter des images (max : 4)</h4>
                                        <input name="file[]" type="file" id="file"/><br><br>
                                        <input type="button" id="add_more" class="upload" value="Ajouter une image"/>
                                    <br>
                                    <br>
                                    <br>
                                    <input type="hidden" value="1" name="active">
                                    <input type="submit" value="Ajouter l'article" name="submit" id="upload" class="upload" style="float: right"/>
                                    <!-- Text input-->
                                </fieldset>
                            </form>
                </section>
            </div>
<!--
            <div class="col-lg-6">
                <section>
                    <legend>Ajouter des images (Maximum 4)</legend>
                    <div id="formdiv">
                        <form enctype="multipart/form-data" action="index.php?action=add_article" method="post">
                            <div id="filediv"><input name="file[]" type="file" id="file"/></div>
                            <br>
                            <input type="button" id="add_more" class="upload" value="Ajouter une image"/>
                            <input type="submit" value="Ajouter les images" name="submit" id="upload" class="upload"/>
                        </form>
                    </div>
                </section>
            </div>
-->
        </div>
    </div>
</main>

 <!--<form action="/file-upload" method="post" class="dropzone" id="my-awesome-dropzone" enctype="multipart/form-data">
        <div class="fallback">
           <input name="file" type="file" multiple />
              </div>
           <input type="submit">!-->

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>

<script>

    $(function () {
        var contenance = document.getElementById("contenance");
        var taille = document.getElementById("taille");
        var pointure = document.getElementById("pointure");

        contenance.hidden = true;
        taille.hidden = false;
        pointure.hidden = true;
    });



    var abc = 0;      // Declaring and defining global increment variable.
    var max = 0;      // Nombre maximum d'image
    $(document).ready(function(){
        //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
            $('#add_more').click(function () {
                if (max <=2) {
                    //and the imageview css could look something like
                    this.classList.add("imageview")
                    $(this).before($("<div/>", {
                        id: 'filediv'
                    }).fadeIn('slow').append($("<input/>", {
                        name: 'file[]',
                        type: 'file',
                        id: 'file'
                    }), $("<br/><br/>")));
                    max++;
                }});

        // Following function will executes on change event of file input to select different file.
        $('body').on('change', '#file', function() {
            if (this.files && this.files[0]) {
                abc += 1; // Incrementing global variable by 1.
                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                $(this).hide();
                $("#abcd" + abc).append($("<img/>", {
                    id: 'img',
                    src: 'x.png'
                }).click(function() {
                    $(this).parent().parent().remove();
                    max--;
                }));
            }
        });
        // To Preview Image
        function imageIsLoaded(e) {
            $('#previewimg' + abc).attr('src', e.target.result);
        };
        $('#upload').click(function(e) {
            var name = $(":file").val();
            if (!name) {
                alert("First Image Must Be Selected");
                e.preventDefault();
            }
        });
    });

</script>