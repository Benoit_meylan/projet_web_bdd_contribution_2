<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 14.06.2018
 * Time: 10:29
 */
// ouvre la mémoire tampon
ob_start();
?>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
            <div class="row">
                <div class="col-md-6">
                    <section class="section sign-in inner-right-xs">
                        <h3>Votre produit a bien été mis à jour!</h3>
                    </section>
                </div>
            </div>
        </div>
    </main>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>