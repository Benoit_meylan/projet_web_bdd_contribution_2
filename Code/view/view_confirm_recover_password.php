<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 20.06.2018
 * Time: 15:44
 */
ob_start();
?>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
            <div class="row">
                <div class="col-md-6">
                    <section class="section sign-in inner-right-xs">
                        <h3>Votre nouveau mot de passe vous a été envoyé par email à l'addresse : <?=$_POST['email']?></h3>
                    </section>
                </div>
            </div>
         </div>
    </main>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>