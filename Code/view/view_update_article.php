<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 17.05.2018
 * Time: 10:04
 */
ob_start();
?>
    <head>
        <meta charset="utf-8">
        <title>hapy - modifier un produit</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-slider.min.css">

        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

        <script src="js/bootstrap-slider.min.js"></script>

        <link rel="stylesheet" href="css/style.css">
    </head>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-12 sidebar-filter">
            <div class="col-md-8 col-lg-12">

                <section class="products">
                    <div class="container">
                        <table class="table">
                            <tr>
                                <th>nom</th>
                                <th>prix</th>
                                <th>catégorie</th>
                                <th>gérer le stock </th>
                                <th>modifier</th>
                                <th>désactiver</th>
                            </tr>
                            <?php foreach ($products as $item):?>
                                <?php
                                    if($item['active'])
                                    {
                                        $label = "Désactiver";
                                        $value = 0;
                                    }
                                    else
                                    {
                                        $label = "Activer";
                                        $value = 1;
                                    }

                                ?>
                                <tr>
                                    <td><?=$item['nom']?></td>
                                    <td><?=$item['prix']?></td>
                                    <td><?=$item['categorie']?></td>
                                    <td><a href="index.php?action=view_stock&id=<?=$item['idProduits']?>&category=<?=$item['categorie']?>">gérer</a></td>
                                    <td><a href="index.php?action=view_update_data_product&id=<?=$item['idProduits']?>&category=<?=$item['categorie']?>">modifier</a></td>
                                    <td><a href="index.php?action=remove_item&id=<?=$item['idProduits']?>&actif=<?=$value?>"><?=$label?></a></td>
                                </tr>
                            <?php endforeach;?>

                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    <script type="text/javascript">


        // Without JQuery
        var slider = new Slider('#ex2', {
        });
        slider.on("slide", function(sliderValue) {
            document.getElementById("price-min").value = sliderValue[0];
        });
        slider.on("slide", function(sliderValue) {
            document.getElementById("price-max").value = sliderValue[1];
        });

    </script>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>