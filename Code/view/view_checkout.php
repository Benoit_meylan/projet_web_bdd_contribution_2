<?php
/**
 * Created by PhpStorm.
 * User: Leo.ZMOOS
 * Date: 31.05.2018
 * Time: 10:10
 */
ob_start();
?>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" >
            <div class="row">
                <div class="col-lg-12 text-center" >
                    <section class="section sign-in inner-right-xs">
                        <legend>Sélectionner votre moyen de paiement</legend>
                        <form class="well form-horizontal" enctype="multipart/form-data" action="index.php?action=add_article" method="post">
                            <fieldset>
                                <table class="table" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;">
                                    <tr>
                                        <td style="border: solid 1px black">
                                            <h1><u>IBAN</u></h1>
                                            <h4>Voici ci-dessous les informations nécessaires au paiement</h4>
                                            <form>
                                                <label class="col-md-12 control-label">IBAN:</label>
                                                <input type="text" name="pays" value="CH52 0483 5012 3456 71000" size="60px" disabled><br>
                                                <label class="col-md-12 control-label">Pays de la banque bénéficiaire:</label>
                                                <input type="text" name="pays" value="Suisse" size="60px" disabled><br>
                                                <label  class="col-md-12 mt-1 control-label">Destinataire:</label>
                                                <input type="text" name="Destinataire" value="Hapy Corporation SA." size="60px" disabled>
                                            </form>
                                            <br>
                                            <br>
                                            <h1><u>Paypal</u></h1><br>
                                            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

                                                <!-- Identify your business so that you can collect the payments. -->
                                                <input type="hidden" name="business" value="leo-facilitator@zmoos.net">

                                                <!-- Specify a Buy Now button. -->
                                                <input type="hidden" name="cmd" value="_xclick">

                                                <!-- Specify details about the item that buyers will purchase. -->
                                                <input type="hidden" name="item_name" value="">
                                                <input type="hidden" name="amount" value="<?= $_SESSION['total']?>">
                                                <input type="hidden" name="currency_code" value="CHF">
                                                <input type="hidden" name="return" value="https://www.hapy.mycpnv.ch/index.php?action=add_command">
                                                <input type="hidden" name="cancel_return" value="https://www.hapy.mycpnv.ch/index.php?action=home">
                                                <input type="hidden" name="notify_url" value="https://www.hapy.mycpnv.ch/index.php?action=home">

                                                <!-- Display the payment button. -->

                                                <input type="image" name="submit" border="0"
                                                       alt="Paiement avec Paypal" src="assets/CheckoutPaypal.png">
                                                <img alt="" border="0" width="1" height="1"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

                                            </form>

                                        </td>

                                    </tr>
                                </table>
                            </fieldset>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </main>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>