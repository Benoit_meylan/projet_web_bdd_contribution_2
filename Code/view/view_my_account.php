<?php
/**
 * Created by PhpStorm.
 * User: Benoît Meylan & Léo Zmoos
 * Date: 11.05.2018
 * Time:  22:02
 */

$titre = "Hâpy - Ajout d'article";
// ouvre la mémoire tampon
ob_start();

?>
<br>

<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-lg-12">
                <?php if (isset($_GET['msg']) && $_GET['msg'] == "changepwdtrue") echo "<h2><font color='#00008b'><b>Le mot de passe à été mis à jour</font></h2>"?>
                <section class="section sign-in inner-right-xs">
                    <legend>Mon compte</legend>
                    <form class="well form-horizontal" action="index.php?action=update_account&msg=true" method="post">
                        <fieldset>
                            <!-- Form Name -->
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-12 control-label">Informations personnelles</label>
                                <div class="col-md-12 inputGroupContainer">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <label>Nom</label>
                                                <div class="input-group">
                                                    <input  name="nom" placeholder="Insérer votre nom" class="form-control" value="<?= $resultats['nom'] ?>"  type="text" maxlength="45">
                                                </div>
                                            </td>

                                            <td>
                                                <label>Prénom</label>
                                                <div class="input-group">
                                                    <input name="prenom" placeholder="Insérer votre Prénom" class="form-control" value="<?= $resultats['prenom'] ?>"  type="text" maxlength="45">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label>Civilité</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="civilite">
                                                        <option value="m">M.</option>
                                                        <option value="mme">Mme</option>
                                                    </select>
                                                </div>
                                            </td>


                                            <td>
                                                <label>N°Téléphone</label>
                                                <div class="input-group">
                                                    <input name="ntel" placeholder="Insérer votre N°Téléphone" class="form-control" value="<?= $resultats['nTelephone'] ?>"  type="number" max="9999999999">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Informations sur la livraison</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Adresse</label>
                                                <div class="input-group">
                                                    <input name="adresse" placeholder="Insérer votre Adresse" class="form-control" value="<?= $resultats['adresse'] ?>"  type="text" max="100">
                                                </div>
                                            </td>

                                            <td>
                                                <div class="field-row">
                                                    <label>Pays</label>
                                                    <select name='pays' class="form-control">
                                                        <option <?php if ($resultats['pays'] == "Suisse") echo "selected"?>>Suisse</option>
                                                        <option <?php if ($resultats['pays'] == "France") echo "selected" ?>>France</option>
                                                        <option <?php if ($resultats['pays'] == "Allemagne") echo "selected"?>>Allemagne</option>
                                                        <option <?php if ($resultats['pays'] == "Etats-Unis") echo "selected"?>>Etats-Unis</option>
                                                        <option <?php if ($resultats['pays'] == "Canada") echo "selected"?>>Canada</option>
                                                        <option <?php if ($resultats['pays'] == "Australie")  echo "selected"?>>Australie</option>
                                                    </select>
                                                </div><!-- /.field-row -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Ville</label>
                                                <div class="input-group">
                                                    <input name="ville" placeholder="Insérer votre Ville" class="form-control" value="<?= $resultats['ville'] ?>"  type="text" max="100">
                                                </div>
                                            </td>

                                            <td>
                                                <label>NPA</label>
                                                <div class="input-group">
                                                    <input name="npa" placeholder="Insérer votre NPA" class="form-control" value="<?= $resultats['npa'] ?>"  type="number" max="100000">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="index.php?action=changePassword" class="form-control" style="float: right">Changer votre mot de passe</a>
                                            </td>
                                            <td>
                                                <input type="submit" value="Mettre à jour les données" name="submit" id="upload" class="btn" style="float: right"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </section>
            </div>
            <!--
                        <div class="col-lg-6">
                            <section>
                                <legend>Ajouter des images (Maximum 4)</legend>
                                <div id="formdiv">
                                    <form enctype="multipart/form-data" action="index.php?action=add_article" method="post">
                                        <div id="filediv"><input name="file[]" type="file" id="file"/></div>
                                        <br>
                                        <input type="button" id="add_more" class="upload" value="Ajouter une image"/>
                                        <input type="submit" value="Ajouter les images" name="submit" id="upload" class="upload"/>
                                    </form>
                                </div>
                            </section>
                        </div>
            -->
        </div>
    </div>
</main>

<!--<form action="/file-upload" method="post" class="dropzone" id="my-awesome-dropzone" enctype="multipart/form-data">
       <div class="fallback">
          <input name="file" type="file" multiple />
             </div>
          <input type="submit">!-->

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>