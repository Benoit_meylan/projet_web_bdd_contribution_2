<?php
/**
 * Created by PhpStorm.
 * User: Leo.ZMOOS
 * Date: 31.05.2018
 * Time: 10:10
 */
ob_start();
?>

<?php $_SESSION['total'] = 0;?>
<?php $nb_article = 0; if (isset($_SESSION['panier'])) $nb_article = count($_SESSION['panier']);?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Aurum - Bootstrap 4 Ecommerce Template</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">

		<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<section class="cart text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 mb-3 mb-m-1 text-md-left"><a href="index.php?action=view_catalog"><i class="fas fa-arrow-left mr-2"></i> Continuer Shopping</a></div>

				</div>
				<div class="row">
					<div class="col-12 text-center">
                        <?php if(isset($_GET['noItem'])):?>
                            <p class="alert-danger">veuillez ajouter au moins 1 article dans votre panier</p>
                        <?php endif;?>

						<h2 class="mt-5 mb-2">Votre Panier</h2>
						<p class="mb-5"><span class="primary-color"><?= $nb_article; ?></span> Items dans votre panier</p>
						<table id="cart" class="table table-condensed" >
							<thead>
								<tr>
									<th style="width:60%">Produit</th>
									<th style="width:12%">Prix</th>
									<th style="width:10%">Quantité</th>
									<th style="width:16%"></th>
								</tr>
							</thead>
							<tbody>
                                <?php
                                    if(isset($_SESSION['panier']))
                                    {
                                        $i = -1;
                                        foreach ($_SESSION['panier'] as $panier)
                                        {
                                            $i++;
                                            echo "<tr>
                                                    <td data-th=\"Product\">
                                                        <div class=\"row\">
                                                            <div class=\"col-md-3 text-left\">
                                                                <img src=\"".$panier['lien_image1']."\" alt=\"\" class=\"img-fluid\">
                                                            </div>
                                                            <div class=\"col-md-9 text-left mt-sm-2\">
                                                                <h4>".$panier['nom']."(".$panier['option'].")</h4>
                                                                    <p>".$panier['description']."</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td data-th=\"Price\">".$panier['prix']."</td>
                                                    <td data-th=\"Quantity\">
                                                        <input type=\"number\" disabled class=\"form-control text-center\" value=\"".$panier['quantite']."\">
                                                    </td>
                                                    <td class=\"actions\" data-th=\"\">
                                                    <div class=\"text-right\">
            
                                                       <a href='index.php?action=delete_cart&indice=$i'><button class=\"btn btn-white btn-md mb-2\"><i class=\"fas fa-trash\"></i></button></a>
                                                    </div>
                                                    </td>
                                                </tr>";
                                                $_SESSION['total'] += $panier['quantite'] * $panier['prix'];
                                        }
                                    }
                                ?>

							</tbody>
						</table>
						<div class="float-right text-right">
							<h4>Total d'achat:</h4>
							<h1><?php echo $_SESSION['total'];?> .- CHF</h1>
							<p>(Frais de livraison exclus)</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 mb-3 mb-m-1 text-md-left"><a href="index.php?action=view_catalog"><i class="fas fa-arrow-left mr-2"></i> Continuer Shopping</a></div>
					<div class="col-sm-6 text-md-right"><a href="index.php?action=add_command" class="btn btn-primary btn-lg pl-5 pr-5">paiement</a>
</div>
				</div>
			</div>
		</section>

		<div class="divider"></div>





		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

	</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>