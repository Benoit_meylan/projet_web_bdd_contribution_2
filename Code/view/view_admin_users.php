<?php
/**
 * Created by PhpStorm.
 * User: Leo.ZMOOS
 * Date: 06.06.2018
 * Time: 15:44
 */

$titre = "Hâpy - mon compte";
// ouvre la mémoire tampon
ob_start();

?>
<?php if (isset($_GET['msg']) && $_GET['msg'] == "true") echo "<h1 class=\"alert\"><font color='red'>L'utilisateur a été supprimé</font></h1>";?>
<table class="table col-md-10">
    <tr>
        <td><b><u>Nom</u></b></td>
        <td><b><u>Prenom</u></b></td>
        <td><b><u>Rôles</u></b></td>
        <td><b><u>Supprimer</u></b></td>
        <td><b><u>Modifier</u></b></td>
    </tr>

    <?php foreach ($result as $user){

        echo "<tr>
                <td>
                    ".$user['nom']."
                </td>
                <td>
                    ".$user['prenom']."
                </td>
                <td>
                    ".$user['roles']."
                </td>
                <td>
                    <a href='index.php?action=del_user_admin&id=".$user['idUtilisateurs']."'><img src='assets/delete.jpg'></a>
                </td>
                <td>
                    <select name='role' onchange=\"self.location.href='index.php?action=role_augment&idUser=".$user['idUtilisateurs']."&Role='+this.value;\">
                        <option value='-1'>Choisir un rôle</option>
                        <option value='1'>Client</option>
                        <option value='2'>Vendeur</option>
                        <option value='3'>Administrateur</option>     
                    </select> 
                </td>
              </tr>";
    }
?>
</table>



<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
