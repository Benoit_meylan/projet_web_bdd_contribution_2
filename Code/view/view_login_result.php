<?php
/**
 * Created by PhpStorm.
 * User: benoit.Meylan
 * Date: 13.03.2018
 * Time: 08:45
 */


$titre = "Hâpy - login result";
// ouvre la mémoire tampon
ob_start();
?>
<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-6">
                <section class="section sign-in inner-right-xs">
                    <h3>Vous êtes bien connecté : <?=$_SESSION['utilisateur']['prenom']." ".$_SESSION['utilisateur']['nom'] ?></h3>
                </section>
            </div>
        </div>
    </div>
</main>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
