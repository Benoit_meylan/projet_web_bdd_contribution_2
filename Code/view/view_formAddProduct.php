<?php
/**
 * Created by PhpStorm.
 * User: MEYLANBenoit
 * Date: 22.05.2018
 * Time: 16:24
 */

$titre = "Poseidon - login";
// ouvre la mémoire tampon
ob_start();

?>


    <br>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
            <div class="row">
                <div class="col-md-6">
                    <section class="section sign-in inner-right-xs">
                        <h2 class="bordered">Ajouter un produit</h2>
                        <form role="form" class="form-group" method="post" action="index.php?action=addProduct">
                            <div class="field-row">
                                <label>nom</label>
                                <input type="text" class="form-control" name="mail_sign" value="<?php if (isset($_GET['qMail'])) echo $_GET['qMail'];?>" required>
                            </div>

                            <div class="field-row">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password_sign" required>
                            </div>
                            <br>
                            <div class="field-row clearfix">
                            <span class="pull-left">
                                <a href="#" class="content-color bold"><font color="black">Mot de passe oublié ?</font></a>
                            </span>
                            </div>
                            <br>
                            <div class="buttons-holder">
                                <button type="submit" class="btn btn-default" style="background-color: lightgrey">Connexion</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </main><!-- /.authentication -->
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>