<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 14.06.2018
 * Time: 09:55
 */

$titre = "hapy - infos produits";
// ouvre la mémoire tampon
ob_start();
?>


<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-6">
                <section class="section register inner-left-xs">
                    <h3 class="bordered">Modifier : <?=$item['nom']?></h3>

                    <form role="form" class="form-group" method="post" action="index.php?action=updateProduct">
                        <div class="form-group">
                            <table class="table">
                                <tr>
                                    <td>
                                        <label for="nom" class="col-md-12 control-label">nom</label>
                                        <input name="nom" type="text" class="form-control" value="<?=$item['nom']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="prix" class="col-md-12 control-label">prix</label>
                                        <input name="prix" type="text" class="form-control" value="<?=$item['prix']?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="description" class="col-md-12 control-label">description</label>
                                        <textarea name="description" maxlength="300" class="form-control mb-4" rows="5" style="height: 10em!important;"><?=$item['description']?></textarea>
                                    </td>
                                </tr>


                                <tr>
                                    <input name="id" type="hidden" value="<?=$item['idProduits']?>">
                                    <td><input type="submit" value="modifier" class="btn btn-primary"></td>
                                </tr>
                            </table>
                        </div>
                    </form role="form" class="form-group" method="post" action="index.php?action=addUser">
                </section><!-- /.register -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /.authentication -->
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>



