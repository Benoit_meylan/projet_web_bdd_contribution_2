<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 30.05.2018
 * Time: 14:55
 * détails d'un produit
 */

ob_start();
echo "oui";
print_r($item);

?>
<head xmlns="http://www.w3.org/1999/html">
    <meta charset="utf-8">
    <title>Aurum - Bootstrap 4 Ecommerce Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">

    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    <link rel="stylesheet" href="css/style.css">

    <style>
        /**THE SAME CSS IS USED IN ALL 3 DEMOS**/
        /**gallery margins**/
        ul.gallery{
            margin-left: 3vw;
            margin-right:3vw;
        }

        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }
        .resize{
                height: 600px;
                width: auto;
                margin-left: auto;
                margin-right: auto;
                display: block;
        }

        .resize2{
                    height: 80px;
                    width: auto;
                    margin-left: auto;
                    margin-right: auto;
                    display: block;
        }

        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size,
            add browser prefixes**/
            -ms-transform: scale(2.5);
            -moz-transform: scale(2.5);
            -webkit-transform: scale(2.5);
            -o-transform: scale(2.5);
            transform: scale(2.5);
            position:relative;
            z-index:100;
        }

        /**To keep upscaled images visible on mobile,
        increase left & right margins a bit**/
        @media only screen and (max-width: 768px) {
            ul.gallery {
                margin-left: 15vw;
                margin-right: 15vw;
            }

            /**TIP: Easy escape for touch screens,
            give gallery's parent container a cursor: pointer.**/
            .DivName {cursor: pointer}
        }
    </style>
</head>

<section class="featured-block text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" style="padding-left: 0px;  padding-right: 0px;">
                <div class="container">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php if($item['lien_image1'] != "NULL"):?><li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li><?php endif;?>
                            <?php if($item['lien_image2'] != "NULL"):?><li data-target="#carouselExampleIndicators" data-slide-to="1"></li><?php endif;?>
                            <?php if($item['lien_image3'] != "NULL"):?><li data-target="#carouselExampleIndicators" data-slide-to="2"></li><?php endif;?>
                            <?php if($item['lien_image4'] != "NULL"):?><li data-target="#carouselExampleIndicators" data-slide-to="3"></li><?php endif;?>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <?php if($item['lien_image1'] != "NULL"):?>
                            <div class="carousel-item active">
                                <img class="d-block img-fluid resize card-img-top" style="" src="<?php if(isset($item['lien_image1']))echo $item['lien_image1']?>">
                            </div>
                            <?php endif;?>
                            <?php if($item['lien_image2'] != "NULL"):?>
                            <div class="carousel-item">
                                <img class="d-block img-fluid resize card-img-top" src="<?php if(isset($item['lien_image2']))echo $item['lien_image2']?>">
                            </div>
                            <?php endif;?>
                            <?php if($item['lien_image3'] != "NULL"):?>
                            <div class="carousel-item">
                                <img class="d-block img-fluid resize card-img-top" src="<?php if(isset($item['lien_image3']))echo $item['lien_image3']?>">
                            </div>
                            <?php endif;?>
                            <?php if($item['lien_image4'] != "NULL"):?>
                            <div class="carousel-item">
                                <img class="d-block img-fluid resize card-img-top" src="<?php if(isset($item['lien_image4']))echo $item['lien_image4']?>">
                            </div>
                            <?php endif;?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

						<div class="product-thumbnails">
                            <table class="table">
                                <tr>
                                    <?php if($item['lien_image1'] != "NULL"):?><td><img class="d-block resize2 card-img-top img-fluid" src="<?php if(!empty($item['lien_image1']))echo $item['lien_image1']; else echo "jeanmichel.jpg"?>" onclick="change_image(0)"></td><?php endif;?>
                                    <?php if($item['lien_image2'] != "NULL"):?><td><img class="d-block resize2 card-img-top img-fluid" src="<?php if(!empty($item['lien_image2']))echo $item['lien_image2']; else echo "jeanmichel.jpg"?>" onclick="change_image(1)"></td><?php endif;?>
                                    <?php if($item['lien_image3'] != "NULL"):?><td><img class="d-block resize2 card-img-top img-fluid" src="<?php if(!empty($item['lien_image3']))echo $item['lien_image3']; else echo "jeanmichel.jpg"?>" onclick="change_image(2)"></td><?php endif;?>
                                    <?php if($item['lien_image4'] != "NULL"):?><td><img class="d-block resize2 card-img-top img-fluid" src="<?php if(!empty($item['lien_image4']))echo $item['lien_image4']; else echo "jeanmichel.jpg"?>" onclick="change_image(3)"></td><?php endif;?>
                                </tr>
                            </table>
                            
                        </div>
            </br>

					<div class="col-lg-12 text-center">
						<h2 class="mb-3 mt-0"><?=utf8_encode($item['nom'])?></h2>
						<p class="lead mt-2 mb-3 primary-color"><?=utf8_encode($item['prix'])?></p>
						<h5 class="mt-4">Description</h5>
                    </div>
                <div class="col-lg-12">
                    <form action="index.php?action=add_cart" method="post">
						<p><?=utf8_encode($item['description'])?>
                            <br>
                            <?php if (isset($_GET['erreur']) && $_GET['erreur'] == "quantite") echo "<font color='red'>Quantité non disponible</font>"?>
                        </p>

						<select class="custom-select form-control col-lg-2 mt-4 mb-4" name="option">
                            <?php foreach ($options as $option):?>
                                <option value="<?=$option['options']?>"><?php echo $option['options']." disponible(".$option['stock'].")";?></option>
                            <?php endforeach;?>
                        </select>

                        <input type="number" class="form-control custom-select col-lg-2 mt-4 mb-4" name="quantite" style="margin-left: 10px" placeholder="Quantité"><br>
                        <input name="cat" value="<?=$item['categorie']?>" hidden>
                        <input name="id" value="<?=$item['idProduits']?>" hidden>
                        <input type="submit" class="btn btn-full-width btn-lg btn-outline-primary col-lg-4" size="200px" value="Ajouter au panier">
                    </form><br>

						<!--Quantity: <input type="text" class="form-control quantity mb-4" name="" value="1">-->

            </div>
				</div>
			</div>
        </div>
</section>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>

<script>
    
    function change_image(num) {

        var image = document.getElementById("img_grand");

        var src = "data/pictures/1_" + num.toString() + ".jpg";

        image.src = src;
    }
    
</script>
