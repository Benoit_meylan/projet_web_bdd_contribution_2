<?php
/**
 * Created by PhpStorm.
 * User: Android
 * Date: 02.05.2018
 * Time: 21:13
 */

require "model/model.php";
require "encryption/cryptage.php";


stream_context_set_default([
    'ssl' => [
        'verify_peer' => false,
        'verify_peer_name' => false,
    ]
    ]);

/**
 * récupère la catégorie et renvoie le nom de l'option corréspondante
 * @param $category
 * @return string
 */
function catToOpt($category)
{
    switch($category)
    {
        case "vetements":
            return "taille";
        case "parfum":
            return "contenance";
        case "chaussures":
            return "pointure";
    }
}

/**
 * affiche la page d'accueil
 */
function home()
{
    require "view/view_home.php";
}

/**
 * affiche la page de login
 */
function view_login()
{
    require "view/view_login.php";
}

/**
 * affiche la page de vendeur
 */
function view_seller()
{
    require "view/view_seller.php";
}

/**
 * ajoute un utilisateur
 */
function add_user()
{

    require_once "recaptcha/recaptchalib.php";

    // your secret key
    $secret = "6Lfxl1kUAAAAAEUHyZkYR9obE9OgnLGPv5_Lwl4r";

    // empty response
    $response = null;

    // check secret key
    $reCaptcha = new ReCaptcha($secret);

    // if submitted check response/
    if (isset($_POST["g-recaptcha-response"])) {
        $response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
    }

    if ($response != null && $response->success) {
        //extraction de la variable POST
        extract($_POST);

        //vérifie si l'email est déjà utilisé
        $mailCrypt = Chiffrement::crypt($mail);

        $resultat = compareMail($mailCrypt);


        $mailCompare = $resultat->fetch(PDO::FETCH_ASSOC);


        //si il est utilisé redirige l'utilisateur sur la page de création de compte
        if($mailCompare['eMail'] == $mailCrypt)
        {
            header("Location:index.php?action=view_login&errMail=true&qPrenom=$prenom&qNom=$nom&qTel=$ntel&qAdresse=$adresse&qVille=$ville&qNpa=$npa&qPays=$pays");
        }
        else

        {
            //compare si l'utilisateur a bien entré sont mot de passe deux fois
            if(hash('sha256', $password) == hash('sha256', $password_ctrl))//confirmation du mot de passe : ok
            {

                //chiffrement du mot de passe
                $passCrypt = Chiffrement::crypt($password);

                new_user($nom, $prenom, $civilite, $mailCrypt, $passCrypt, $ntel);

                $_SESSION['utilisateur']['nom'] = $nom;
                $_SESSION['utilisateur']['prenom'] = $prenom;
                $_SESSION['utilisateur']['fkRoles'] = 1;


                //mail de confirmation de cération de compte
                $to = $mail;

                $subject = "Bienvenue sur notre site";

                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=iso-8859-1';

                //mail destinataire, mail source
                $headers[] = 'To: vendeur <'.$mail.'>';
                $headers[] = 'From: Client <'."admin@hapy.mycpnv.ch".'>';

                $message = "Bienvenue sur hapy : vous pouvez vous connecter sur <a href='http://www.hapy.mycpnv.ch/index.php?action=view_login'>ici</a>";

                mail($to, $subject, nl2br($message), implode("\r\n", $headers));

                $mailCrypt = Chiffrement::crypt($_POST['mail']);
                $resultats = get_user($mailCrypt);
                $result = $resultats->fetch(PDO::FETCH_ASSOC);
                $_SESSION['utilisateur'] = $result;

                addAdress($_POST['adresse'],$_POST['pays'],$_POST['ville'],$_POST['npa'],$result['idUtilisateurs']);

                require "view/view_confirm_newAccount.php";
            }
            else //confirmation du mot de passe : fausse, on le renvoie dans view_login
            {
                header("location:index.php?action=view_login&errPassword=true&qPrenom=$prenom&qNom=$nom&qMail=$mail&qTel=$ntel&qAdresse=$adresse&qVille=$ville&qNpa=$npa&qPays=$pays");
            }

        }

    }
    else
    {
        header("Location:index.php?action=view_login&errCaptcha=true&qPrenom=$prenom&qNom=$nom&qMail=$mail&qTel=$ntel&qAdresse=$adresse&qVille=$ville&qNpa=$npa&qPays=$pays");
    }


}

/**
 * connexion d'un utilisateur
 */
function signin()
{
    extract($_POST);

    if(empty($mail_sign) || empty($password_sign))
    {
        header("location:index.php?action=view_login&errLoginVide=true&qMail=$mail_sign");
    }
    else
    {

        $mailCrypt = Chiffrement::crypt($mail_sign);
        $passCrypt = Chiffrement::crypt($password_sign);
        $resultats = get_user($mailCrypt);
        $result = $resultats->fetch(PDO::FETCH_ASSOC);
        if(empty($result))
        {
            header("location:index.php?action=view_login&errLogin=true&qMail=$mail_sign");
        }
        else
        {
            if($passCrypt == $result['motDePasse'] && $mailCrypt == $result['eMail'])
            {
                $_SESSION['utilisateur'] = $result;

                require "view/view_login_result.php";
            }
            else
            {
                header("location:index.php?action=view_login&errLogin=true&qMail=$mail_sign");
            }
        }
    }
}

/**
 * affiche la page d'un utilisateur
 */
function view_account()
{
    require "view/view_account.php";
}

/**
 * affiche la confirmation qu'un utilisateur c'est déconnecter
 */
function view_disconnect()
{
    require "view/view_disconnect.php";
}

/**
 * affiche les produits dans la page "tous les articles"
 */
function view_catalog()
{

    if (empty($_POST['price'])){$ints[0] = 0; $ints[1] = 10000;}
    else {
    $s = $_POST['price'];
    $ints = array_map('intval', explode(',', $s));
}
    //recupère les articles par catégorie pour faciliter le tri
    if (!isset($_POST['categorie'])) $_POST['categorie'] = "All";
    switch ($_POST['categorie'])
    {

        case 'clothes':
            $result_c = getClothes($ints[0],$ints[1]);
            $active = getActiveItemByCat($_POST['categorie']);
            $activeItem = $active->fetch(PDO::FETCH_ASSOC);
            $clothes = $result_c -> fetchAll(PDO::FETCH_ASSOC); break;

        case 'perfumes':
            $result_p = getPerfume($ints[0],$ints[1]);
            $active = getActiveItemByCat($_POST['categorie']);
            $activeItem = $active->fetch(PDO::FETCH_ASSOC);
            $perfume = $result_p -> fetchAll(PDO::FETCH_ASSOC); break;

        case 'shoes':
            $result_s = getShoes($ints[0],$ints[1]);
            $active = getActiveItemByCat($_POST['categorie']);
            $activeItem = $active->fetch(PDO::FETCH_ASSOC);
            $shoes = $result_s -> fetchAll(PDO::FETCH_ASSOC); break;

        case 'All':
            $result_c = getClothes($ints[0],$ints[1]);
            $clothes = $result_c -> fetchAll(PDO::FETCH_ASSOC);
            $result_p = getPerfume($ints[0],$ints[1]);
            $perfume = $result_p -> fetchAll(PDO::FETCH_ASSOC);
            $result_s = getShoes($ints[0],$ints[1]);
            $active = getActiveItemByCat($_POST['categorie']);
            $activeItem = $active->fetch(PDO::FETCH_ASSOC);
            $shoes = $result_s -> fetchAll(PDO::FETCH_ASSOC); break;

    }

    require "view/view_catalog.php";


}


/**
 * affiche la page de création d'article
 */
function view_add_article()
{
    if (isset($_SESSION['utilisateur']))
    {
        //si l'utilisateur est un vendeur il peut créer un produit
        if ($_SESSION['utilisateur']['fkRoles'] == 2)
        {
            $resultats = getCategories();

            require "view/view_add_article.php";
        }
    }
}

/**
 * ajoute un article
 */
function add_article()
{
    extract($_POST);

    //ajouter le produit dans la table produit
    add_Product($category, $name, $price, $description, $active);

    switch ($category)
    {
        case 1:
            $optionName = "taille";
            $optionValue = $taille;
            break;
        case 2:
            $optionName = "contenance";
            $optionValue = $contenance;
            break;
        case 3:
            $optionName = "pointure";
            $optionValue = $pointure;
            break;
    }


    $resultat = getLastProductId();
    $productId = $resultat->fetch(PDO::FETCH_ASSOC);
    $product = $productId['id'];
    $resultat = getOptionId($optionValue, $optionName);
    $optionId = $resultat->fetch(PDO::FETCH_ASSOC);
    $option = $optionId['id'.ucfirst($optionName)];



    //ajoute le stock d'un produit dans sa table correspondante
    add_Stock($option, $product, $stock, $optionName);

    //gestion des images
    $dataDirectory = "data/pictures";

    for($i = 0; $i < 4; $i++)
    {
        $path[$i] = "NULL";
    }
    for($i = 0; $i < count($_FILES['file']['name']); $i++)
    {
        $fileName[$i] = $product."_".$i.strrchr($_FILES['file']['name'][$i], "."); //nom de l'imgae
        $path[$i] = $dataDirectory."/".$fileName[$i];//chemin de l'imgae
        move_uploaded_file($_FILES['file']['tmp_name'][$i],$path[$i]);//upload l'image sur le serveur

    }

    add_pictures($path, $product);

    require "view/view_confirm_newArticle.php";
}

/**
 * détail d'un article
 */
function view_detail()
{
    //recupère les détails d'un produit
    $result = getItem($_GET['id'], $_GET['category']);
    $result2 = getOptions($_GET['id'],$_GET['category']);

    $item = $result->fetch(PDO::FETCH_ASSOC);
    $options = $result2->fetchALL(PDO::FETCH_ASSOC);


    require "view/view_detail.php";

}

/**
 * affiche le panier
 */
function view_cart()
{
    require "view/view_cart.php";
}

/**
 * ajouter un produit au panier
 */
function add_cart()
{
    $result = getItem($_POST['id'], $_POST['cat']);
    $item = $result->fetch(PDO::FETCH_ASSOC);

    $result = getOptions($_POST['id'], $_POST['cat']);
    $options = $result->fetchAll(PDO::FETCH_ASSOC);

    $newItem = true;

    //on récupère la valeur du stock de l'option choisie
    foreach ($options as $option)
    {
        if($_POST['option'] == $option['options'])
        {
            $resultOption = $option;
            break;
        }
    }

    if($_POST['quantite']>$item['stock'])
    {
        header("Location:index.php?action=view_detail&id=".$_POST['id']."&category=".$_POST['cat']."&erreur=quantite");
    }
    else
    {
        if(!isset($_SESSION['panier']))
        {
            $_SESSION['panier'] = array();
        }


        for($i = 0; $i < count($_SESSION['panier']); $i++)
        {
            //vérifie si un produit avec la même option existe déjà dans le panier si oui les concatènent
            if($_SESSION['panier'][$i]['id'] == $_POST['id'] && $_SESSION['panier'][$i]['option'] == $_POST['option'])
            {
                $_SESSION['panier'][$i]['quantite'] += $_POST['quantite'];
                $newItem = false;

                //si la quantité demandée est supérieur au stock renvoie l'utilisateur avec une erreur
                if ($_SESSION['panier'][$i]['quantite']>$item['stock']) {
                    $_SESSION['panier'][$i]['quantite'] -= $_POST['quantite'];
                    header("Location:index.php?action=view_detail&id=".$_POST['id']."&category=".$_POST['cat']."&erreur=quantite");
                }

            }
        }

        if($newItem)
        {
            array_push($_SESSION['panier'], array("id"=>$_POST['id'], "nom"=>$item['nom'], "category"=>$_POST['cat'],"prix"=>$item['prix'],"description"=>$item['description'], "lien_image1"=>$item['lien_image1'], "quantite"=>$_POST['quantite'], "option"=>$resultOption['options']));
        }




        /*aide pour debug
        print_r($_POST);
        echo "<br>===<br>";
        print_r($item);
        echo "<br>===<br>options : ";
        print_r($options);
        echo "<br>===<br>resultOpt : ";
        print_r($resultOption);
        echo "<br>============";
        echo "<pre>";
        print_r($_SESSION['panier']);
        echo "</pre>";
        */


        require "view/view_cart.php";
    }
}

/**
 * page de gestion d'un article
 */
function view_update_article()
{
    $results = getProducts();
    $products = $results->fetchAll(PDO::FETCH_ASSOC);
    require "view/view_update_article.php";

}

/**
 * gestion du stock
 */
function view_stock()
{
    $result = getItem($_GET['id'],$_GET['category']);
    $item = $result->fetchAll(PDO::FETCH_ASSOC);

    require "view/view_stock.php";
}

/**
 * page administrateur
 */
function view_admin()
{
    require "view/view_admin.php";
}

/**
 * affiche tous les utilisateurs
 */
function view_admin_article()
{
    $users = getAllUser();

    $result = $users->fetchAll(PDO::FETCH_ASSOC);

    require "view/view_admin_users.php";
}

function add_user_admin()
{

}

function del_user_admin()
{
    del_user($_GET['id']);
    header("location:index.php?action=view_admin_article&msg=true");
}

/**
 * change le rôle d'un utilisateur
 */
function role_augment(){
    roles_augment($_GET['idUser'],$_GET['Role']);
    view_admin_article();
}

/**
 * désactiver/activer un produit
 */
function remove_item()
{
    extract($_GET);

    removeItem($id, $actif);

    view_update_article();

}

/**
 * modifier le stock
 */
function update_stock()
{
    extract($_POST);


    $optionName = catToOpt($category);

    $optionId = array();

    for($i = 0; $i < count($optionValue);$i++)
    {
        $result = getOptionId($optionValue[$i], $optionName);
        $optionId[$i] = $result->fetch(PDO::FETCH_ASSOC);


        updateStock($id, $optionName, $stock[$i], $optionId[$i]['id'.ucfirst($optionName)]); //attention pour diminuer le stock il faut entrer un nombre négatif

    }

    require "view/view_confirm_update_stock.php";
}

/**
 * affiche la page de modification d'un produit
 */
function view_update_data_product()
{
    $result = getItem($_GET['id'],$_GET['category']);
    $item = $result->fetch(PDO::FETCH_ASSOC);

    require "view/view_update_data_product.php";
}

/**
 * modifie un produit
 */
function update_product()
{
    extract($_POST);

    updateProduct($id, $nom, $prix, $description);

    require "view/view_confirm_update_product.php";
}

/**
 * Fonction pour créer une commande et son détail
 */
function add_command()
{
    if(!isset($_SESSION['utilisateur']))
    {
        header("Location:index.php?action=view_login");
    }

    if(!isset($_SESSION['panier']))
    {
        header("Location:index.php?action=view_cart&noItem=true");

    }

    // Crée la commande
    create_command($_SESSION['utilisateur']['idUtilisateurs']);
    $result = getIdCommand($_SESSION['utilisateur']['idUtilisateurs']);
    $idCommand = $result->fetch(PDO::FETCH_ASSOC);
    $size = sizeof($_SESSION['panier']);
    $id = $idCommand['idCommande'];
    // Ajout du détail des commandes
    foreach ($_SESSION['panier'] as $item) {

        $result = getOptionId($item['option'], catToOpt($item['category']));
        $idOption = $result->fetch(PDO::FETCH_ASSOC);

        addCommandDetail($id,$item['id'],$item['quantite'], $item['option']);
    }
    // Vide le panier
    unset($_SESSION['panier']);

    require "view/view_checkout.php";
}

/**
 * ajoute une option (taille, pointure, etc...)
 */
function add_option()
{
   extract($_POST);

   switch ($category)
   {
       case "vetements":
           $option = $taille;
           $tblName = "taille";
           break;
       case "chaussures":
           $option = $pointure;
           $tblName = "pointure";
           break;
       case "parfum":
           $option = $contenance;
           $tblName = "contenance";
           break;
   }

    $resultat = getOptionId($option, $tblName);
    $fkOption = $resultat->fetch(PDO::FETCH_ASSOC);

    addOption($id, $fkOption['id'.ucfirst($tblName)], $stock, $tblName);

    require "view/view_confirm_add_option.php";
}

function delete_cart()
{
    array_splice($_SESSION['panier'],$_GET['indice'],1);
    require "view/view_cart.php";
}

function view_commands()
{
    $results = getCommands();
    $resultats = $results->fetchAll(PDO::FETCH_ASSOC);

    require "view/view_commands.php";
}


function view_recover_password()
{
    require "view/view_recover_password.php";
}

/**
 * Permet la récupération d'un mot de passe et l'envoie par mail
 */
function recover_password()
{
    //création d'un nouveau mot de passe
    $string = "abcdefghijklmnopqrstuvwxyz0123456789";
    $nPass = str_shuffle($string);
    $nPass = substr($nPass, 0,7);

    $nPassCrypt = Chiffrement::crypt($nPass);

    echo $nPass;
    echo "<br>";
    echo Chiffrement::crypt($nPass);

    updatePassword(Chiffrement::crypt($_POST['email']), $nPassCrypt);

    //mail de confirmation de cération de compte
    $to = $_POST['email'];

    $subject = "Votre mot de passe a été changé !";

    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=UTF-8';

    //mail destinataire, mail source
    $headers[] = 'To: <'.$_POST['email'].'>';
    $headers[] = 'From: <'."admin@hapy.mycpnv.ch".'>';

    $message = "   
                    <h2>Votre mot de passse a été changé</h2>
                    <br>
                    <p>Votre nouveau mot de passe est : <b>".$nPass."</b></p>
                    <p>Vous pouvez vous connectez en cliquant : <a href='http://www.hapy.mycpnv.ch/index.php?action=view_login'>ici</a></p>
                    <p>Nous vous conseillons de changer votre mot de passe, pour cela rendez vous dans la page \"profil\" de votre compte </p>
                ";

    mail($to, $subject, nl2br($message), implode("\r\n", $headers));

    require "view/view_confirm_recover_password.php";
}

/**
 * affiche tous les produits d'une commande
 */
function view_detail_command()
{

    $resultats = getCommandDetail($_GET['idCommand']);
    $product = $resultats->fetchAll(PDO::FETCH_ASSOC);

    require "view/view_detail_command.php";
}

function account()
{
    $results = getUserDetails($_SESSION['utilisateur']['idUtilisateurs']);
    $resultats = $results->fetch(PDO::FETCH_ASSOC);
    require "view/view_my_account.php";
}

/**
 * modifie les information d'un utilisateur
 */
function update_account()
{
    $results = getUserDetails($_SESSION['utilisateur']['idUtilisateurs']);
    $resultats = $results->fetch(PDO::FETCH_ASSOC);

    extract($_POST);

    updateAccount($adresse,$pays,$ville,$npa,$nom,$prenom,$civilite,$ntel,$_SESSION['utilisateur']['idUtilisateurs']);

    $_SESSION['utilisateur']['prenom'] = $prenom;
    $_SESSION['utilisateur']['nom'] = $nom;

    switch ($_SESSION['utilisateur']['fkRoles'])
    {
        case 1:
            require "view/view_account.php";
            break;
        case 2:
            require "view/view_seller.php";
            break;
        case 3:
            require "view/view_admin.php";

    }

}

/**
 * affiche la page de modification de mot de passe
 */
function changePassword()
{
    require "view/view_change_password.php";
}

/**
 * changer le mot de passe
 */
function updatePwd()
{
    $result = getPassword($_SESSION['utilisateur']['idUtilisateurs']);
    $oldpwd = $result->fetch(PDO::FETCH_ASSOC);

    $oldpwdin =Chiffrement::crypt($_POST['oldpwd']);




    if ($oldpwd['motDePasse'] != $oldpwdin)
    {
        header("Location:index.php?action=changePassword&msg=errorold");
    }
    else if ($_POST['newpwd'] != $_POST['confnewpwd'])
    {
        header("Location:index.php?action=changePassword&msg=errorconf");
    }
    else
    {
        $newpwd = Chiffrement::crypt($_POST['newpwd']);
        updatePassword($_SESSION['utilisateur']['eMail'],$newpwd);
        header("Location:index.php?action=account&msg=changepwdtrue");
    }

}

/**
 * affiche la page "nous contacter"
 */
function view_contactUs()
{
    require "view/view_contactUs.php";

}




