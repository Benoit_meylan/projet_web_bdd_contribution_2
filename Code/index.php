<?php
session_start();
/**
 * Created by PhpStorm.
 * User: Leo.ZMOOS
 * Date: 05.02.2018
 * Time: 09:14
 * Index.php : page de triage des actions reçues dans l'URL
 */

require "controler/controler.php";

try
{

    if (isset($_GET['action']))
    {
        $action = $_GET['action'];
        // Sélection de l'action passée par l'URL
        switch ($action)
        {
            case 'home': home(); break;

            case 'view_login': view_login(); break;

            case 'view_seller': view_seller(); break;

            case 'addUser': add_user(); break;

            case 'signin': signin(); break;

            case 'my_account': view_account(); break;

            case 'view_disconnect': view_disconnect();  break;

            case 'view_catalog': view_catalog(); break;

            case 'view_add_article': view_add_article(); break;

            case 'add_article': add_article(); break;

            case 'view_detail':view_detail(); break;

            case 'view_cart': view_cart(); break;

            case 'add_cart': add_cart(); break;

            case 'view_update_product': view_update_article(); break;

            case 'view_admin': view_admin(); break;

            case 'view_admin_article': view_admin_article(); break;

            case 'del_user_admin': del_user_admin(); break;

            case 'role_augment': role_augment(); break;

            case 'view_stock': view_stock(); break;

            case 'remove_item': remove_item(); break;

            case 'updateStock': update_stock(); break;

            case 'add_command': add_command(); break;

            case 'view_update_data_product': view_update_data_product(); break;

            case 'updateProduct' : update_product(); break;

            case 'add_option' : add_option(); break;

            case 'delete_cart': delete_cart(); break;

            case 'view_commands': view_commands(); break;

            case 'view_recover_password' : view_recover_password(); break;

            case 'recover_password':recover_password(); break;

            case 'view_detail_command':view_detail_command(); break;

            case 'account': account(); break;

            case 'update_account': update_account(); break;

            case 'changePassword': changePassword(); break;

            case 'view_contactUs': view_contactUs(); break;

            case 'updatePwd': updatePwd(); break;

            default: throw new Exception("Action non valide");
        }
    }
    else
        home();
}
catch (Exception $e)
{
    erreur($e->getMessage());
}